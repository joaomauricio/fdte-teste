import axios from 'axios';

const API = axios.create({baseURL:'https://pokeapi.co/api/v2/'});

// Range de pokémons que devem ser procurados, definido nos reqs do sistema.
const MIN = 1, MAX = 807;

export function GetRandomPokemon () {
    const pokemonID = getRandomNumber(MIN,MAX);
    return API.get(`pokemon/${pokemonID}`);
}

const getRandomNumber = (min, max) => {
    return Math.floor(Math.random() * (max - min) + 1) + min;
}
