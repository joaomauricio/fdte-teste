import React from 'react';
import { Route, Switch } from 'react-router-dom';
import MapPage from './screens/Map/MapPage';
import Home from './screens/Home/Home';

const App = () => (
    <Switch>
        <Route path="/map" component={MapPage} />
        <Route path="/" component={Home} />
    </Switch>
);

export default App;
