import React, {Component} from 'react';
import Sidebar from '../../components/Sidebar';
import Modal from '../../components/Modal';
import PokemonInfo from '../../components/PokemonInfo';
import PokemonCreate from '../../components/PokemonCreate';
import {GetRandomPokemon} from '../../services/PokemonAPI';

import ashFront from '../../assets/images/ashFront.png';
import ashLeftLeg from '../../assets/images/ashLeftLeg.png';
import ashRightLeg from '../../assets/images/ashRightLeg.png';
import defaultIconImage from '../../assets/images/camera.png';
import searchTooltip from '../../assets/images/searchTooltip.png';
import searchingTooltip from '../../assets/images/searchingTooltip.png';
import errorTooltip from '../../assets/images/tooltipError.png';

const walkingAsh = [ashLeftLeg, ashRightLeg];

/**
    Não tive a disponibilidade de tempo que eu pensei que teria, por isso,
    a implementação não ficou como gostaria. Decidi enviar para não furar o
    prazo combinado, mas estou ainda ativamente atualizando o repositório.
**/
class MapPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            modalContent: <div/>,
            showModal: false,
            modalImage: defaultIconImage,
            currentAsh: ashFront,
            bag: [],
            pokemonFounded: {},
            tooltip: <div style={{width:'7.2rem', height:'6.7rem'}}/>,
            loading: false
        }
        this.animationInterval = undefined;

        this.onClickBag = this.onClickBag.bind(this);
        this.onClickCreate = this.onClickCreate.bind(this);
        this.onClickAsh = this.onClickAsh.bind(this);
        this.onMouseOverAsh = this.onMouseOverAsh.bind(this);
        this.onMouseOutAsh = this.onMouseOutAsh.bind(this);
        this.onModalClose = this.onModalClose.bind(this);
        this.onCapture = this.onCapture.bind(this);
        this.onRelease = this.onRelease.bind(this);
        this.onEditName = this.onEditName.bind(this);
        this.removePokemonFromBag = this.removePokemonFromBag.bind(this);
        this.showModal = this.showModal.bind(this);
        this.showToolTip = this.showToolTip.bind(this);
        this.hideToolTip = this.hideToolTip.bind(this);
        this.setModalImage = this.setModalImage.bind(this);
        this.getPokemon = this.getPokemon.bind(this);
        this.animation = this.animation.bind(this);
        this.stopAnimation = this.stopAnimation.bind(this);
    }

    onCapture(pokemon) {
        this.setState((prevState) => ({bag:[...prevState.bag, pokemon]}));
        this.onModalClose();
    }

    onClickAsh() {
        if (this.state.bag.length < 6) {
            return this.getPokemon();
        }
    }

    onMouseOverAsh() {
        if (this.state.bag.length < 6) {
            this.showToolTip(searchTooltip);
        } else {
            this.showToolTip(errorTooltip);
        }
    }

    onMouseOutAsh() {
        this.hideToolTip();
    }

    onClickCreate() {
        const modalContent = <PokemonCreate onRelease={this.onRelease} onCapture={this.onCapture}/>;
        this.setModalImage();
        this.showModal(modalContent);
    }

    onClickBag(pokemon) {
        let img = undefined;
        if (pokemon.sprites) {
            img = pokemon.sprites.front_default;
        }
        const modalContent = <PokemonInfo
                                onEditName={this.onEditName}
                                nameBeingEdited={this.state.nameBeingEdited}
                                showStats='true'
                                onRelease={this.onRelease}
                                pokemon={pokemon}/>;
        this.setModalImage(img);
        this.showModal(modalContent);
    }

    onRelease(pokemon) {
        this.removePokemonFromBag(pokemon);
        this.onModalClose();
    }

    onEditName(pokemon) {
        const novaLista = this.state.bag.map((e) => {
            if (e.id === pokemon.id) {
                return pokemon;
            } return e;
        });
        return this.setState(() => ({bag:novaLista}))
    }

    removePokemonFromBag(pokemon) {
        return this.setState(() => {
            return {bag:this.state.bag.filter((e) => e === pokemon ? null:pokemon)};
        })

    }

    showModal(modalContent) {
        this.setState(() => ({modalContent: modalContent}));
        this.setState(() => ({showModal:true}));
    }

    onModalClose() {
        return this.setState(() => ({showModal:false}));
    }

    animation() {
        let i = 0;
        this.animationInterval = setInterval(() => {
            this.setState(() => ({currentAsh: walkingAsh[i % 2]}));
            i++;
        }, 500);
    }

    stopAnimation() {
        clearInterval(this.animationInterval);
        this.setState(() => ({currentAsh: ashFront}));
    }

    showToolTip(tooltip) {
        const toolTipContent = <img className='ash-tooltip' src={tooltip} alt='tooltip'/>
        this.setState(() => ({tooltip:toolTipContent}))
    }

    hideToolTip() {
        if (!this.state.loading) {
            const toolTipContent = <div style={{width:'7.2rem', height:'6.7rem'}}/>
            this.setState(() => ({tooltip:toolTipContent}))
        }
    }

    setModalImage(img) {
        this.setState(() => ({modalImage: img || defaultIconImage}));
    }

    getPokemon() {
        this.setState(() => ({loading:true}));
        this.animation();
        this.showToolTip(searchingTooltip);
        // Set timeout para garantir que a animação aconteça pelo menos
        // uma vez, independente da latencia da requesição.
        return setTimeout(() => {
            return GetRandomPokemon().then((res) => {
                this.setState(() => ({pokemonFounded:res.data}))
                const modalContent = <PokemonInfo onCapture={this.onCapture} pokemon={this.state.pokemonFounded}/>;
                this.setModalImage(res.data.sprites.front_default)
                this.showModal(modalContent);
                this.stopAnimation();
                this.setState(() => ({loading:false}));
                this.hideToolTip();
            })
        }, 1000);
    }

    render() {
        return (
            <>
                <div className='map'>
                    <Sidebar bag={this.state.bag} onClickCreate={this.onClickCreate} onClickBag={this.onClickBag}/>
                    <div className='ash-content'>
                        {this.state.tooltip}
                        <img className='ash'
                            onMouseOut={this.onMouseOutAsh}
                            onMouseOver={this.onMouseOverAsh}
                            onClick={this.onClickAsh}
                            src={this.state.currentAsh}
                            alt='Ash'/>
                    </div>
                    <Modal onModalClose={this.onModalClose} showModal={this.state.showModal} modalImage={this.state.modalImage}>
                        {this.state.modalContent}
                    </Modal>
                </div>
            </>
        );
    }
};

export default MapPage;
