import React from 'react';
import Button from '../../components/Button';
import { Link } from 'react-router-dom';

import pokeLogo from '../../assets/images/pokemonLogo.png'

const Home = () => {
    return (
        <div className='home'>
            <div className='start'>
                <img className='logo' src={pokeLogo} alt='Pokemon'/>
                <Link to='/map'>
                    <Button extraClass='btn-start' text='Start'/>
                </Link>
            </div>
        </div>
    );
}

export default Home;
