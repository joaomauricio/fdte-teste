import React from 'react';

const Button = ({ text, icon, onClick, extraClass, disabled }) => {

    return (
        <button disabled={disabled} onClick={onClick} className={`btn btn--${text ? 'text' : 'icon'} ${extraClass}`}>
            {text || icon}
        </button>
    )
};

export default Button;
