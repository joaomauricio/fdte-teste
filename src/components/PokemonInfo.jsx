import React, {Component} from 'react';
import Button from './Button';
import TextInput from './TextInput';

import pokeball from '../assets/images/pokeball.png';
import shield from '../assets/images/shield.png';
import speed from '../assets/images/speed.png';
import sword from '../assets/images/sword.png';
import editIcon from '../assets/images/editIcon.png';
import checkIcon from '../assets/images/checkIcon.png';
import close from '../assets/images/close.png';

import {pokemonTypes} from '../services/PokemonTypes.js';

const iconsOrder = [shield, sword, shield, sword, speed]

const stats = {
    'attack':'ataque',
    'defense':'defesa',
    'special-defense': 'defesa especial',
    'special-attack': 'ataque especial',
    'speed': 'velocidade'
}

class PokemonInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            nameBeingEdited:false,
            oldName:props.pokemon.name,
            pokemon:props.pokemon
        }

        this.buildLists = this.buildLists.bind(this);
        this.onCaptureAux = this.onCaptureAux.bind(this);
        this.onReleaseAux = this.onReleaseAux.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.onCancelNameChange = this.onCancelNameChange.bind(this);
        this.onConfirmNameChange = this.onConfirmNameChange.bind(this);
        this.startStopTyping = this.startStopTyping.bind(this);
        this.buildLists();
    }

    buildLists() {
        this.types = this.props.pokemon.types.map((e) => {
            const typeClass = `pokemon-stats-col type--${e.type.name}`
            return (
                <div className={typeClass} key={e.slot}>
                    <h2> {pokemonTypes[e.type.name]} </h2>
                </div>
            )
        })

        this.abilities = this.props.pokemon.abilities.map((e) => {
            return (
                <div className='pokemon-stats-col' key={e.slot}>
                    <h3> {e.ability.name} </h3>
                </div>
            )
        })

        // Esse modulo é usado tanto para o modal de capture como para o modal
        // de stats. Por isso, É necessário um condicional para decidir qual
        // botão exibir na tela.
        this.isCaptureButton = this.props.onCapture ? true:false;

        // Quando o pokemon é encontrado, no modal não aparece as estatísitcas,
        // mas no modal de informação quando vc já captrou, sim.
        this.estatisticasTitle = undefined;
        this.estatisticas = undefined;
        if(this.props.showStats) {
            this.estatisticasTitle = (
                <div className='pokemon-stats'>
                    <h2><span>estatisticas</span></h2>
                </div>
            )
            this.estatisticas = this.props.pokemon.stats.slice(1).map((e,i) => {

                return (
                    <div key={i} className='pokemon-stats stats'>
                        <img alt='i' src={iconsOrder[i]} style={{marginRight:'1rem'}}/>
                        <h3>{stats[e.stat.name]}</h3>
                        <h3 style={{marginLeft:'auto'}}>{e.base_stat}</h3>
                    </div>
                )
            })
        }
    }

    onCaptureAux() {
        this.props.onCapture(this.props.pokemon);
    };

    onReleaseAux() {
        this.props.onRelease(this.props.pokemon);
    }

    startStopTyping() {
        this.setState((prevState) => ({nameBeingEdited:!prevState.nameBeingEdited}));
    }

    handleChange(event) {
        const value = event.target.value;
        this.setState((prevState) => ({
            pokemon:{...prevState.pokemon,
            name:value}
        }))
    }

    onConfirmNameChange() {
        this.props.onEditName(this.state.pokemon);
        this.startStopTyping();
        this.setState(() => ({oldName:this.state.pokemon.name}));
    }

    onCancelNameChange() {
        const oldName = this.state.oldName;
        return this.setState((prevState) => ({
            pokemon:{
                ...prevState.pokemon,
                name:oldName
            },
            nameBeingEdited:false

        }))
    }

    render() {
        return (
            <>
                <div className='pokemon-name'>
                    {(this.state.nameBeingEdited && (
                        <>
                        <TextInput onChange={this.handleChange} name='name' value={this.state.pokemon.name}/>
                        <div onClick={this.onConfirmNameChange} className='edit-name'>
                            <img alt='i' className='edit-name-icon' src={checkIcon}/>
                        </div>
                        <div onClick={this.onCancelNameChange} className='edit-name'>
                            <img alt='i' className='edit-name-icon' src={close}/>
                        </div>
                        </>
                    )) || (
                        <h1> {this.state.pokemon.name} </h1>
                    )}
                    {(this.props.showStats && !this.state.nameBeingEdited) &&(
                        <img onClick={this.startStopTyping} style={{marginLeft:'1rem'}} src={editIcon} alt='capture'/>
                    )}
                </div>
                <div className='pokemon-stats'>
                    <div className='pokemon-stats-col'>
                        <h2 className='stats-type'> HP </h2>
                        <h1> {this.props.pokemon.stats[0].base_stat} / {this.props.pokemon.stats[0].base_stat} </h1>
                    </div>
                    <div className='pokemon-stats-col'>
                        <h2 className='stats-type'> ALTURA </h2>
                        <h1> {this.props.pokemon.height / 10} m </h1>
                    </div>
                    <div className='pokemon-stats-col'>
                        <h2 className='stats-type'> PESO </h2>
                        <h1> {this.props.pokemon.weight / 10} kg </h1>
                    </div>
                </div>
                <div className='pokemon-stats'>
                    <h2><span>tipo</span></h2>
                </div>
                <div className='pokemon-stats after-title'>
                    {this.types}
                </div>
                <div className='pokemon-stats'>
                    <h2><span>habilidades</span></h2>
                </div>
                <div className='pokemon-stats after-title'>
                    {this.abilities}
                </div>
                { this.props.showStats && (
                    <>
                        {this.estatisticasTitle}
                        {this.estatisticas}
                    </>
                )

                }
                <div className='pokemon-stats capture'>
                    { (this.isCaptureButton && (
                        <img onClick={this.onCaptureAux} src={pokeball} alt='capture'/>
                    )) ||
                        <Button onClick={this.onReleaseAux} text='Liberar Pokemon' extraClass='create-btn'/>
                    }
                </div>
            </>
        );
    }
};

export default PokemonInfo;
