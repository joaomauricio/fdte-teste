import React from 'react';
import PropTypes from 'prop-types';
import closeIcon from '../assets/images/close.png';

const Modal = (props) => {

    const toggleModal = () => {
        props.onModalClose();
    }

    if (props.showModal){
        return (
            <div className="modal">
                <div className="modal__content">
                    <img  onClick={toggleModal} className="modal__close" src={closeIcon} alt="Fechar" />
                    <div className="image-card">
                        <div className="rounded-circle-image">
                            <img src={props.modalImage} alt='close'/>
                        </div>
                    </div>
                    <div className="card">
                        <div className='pokemon-info'>
                            <div className='pokemon-info-content'>
                                {props.children}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    } else {
        return <></>;
    }


}


Modal.propTypes = {
    children: PropTypes.node.isRequired,
};

export default Modal;
