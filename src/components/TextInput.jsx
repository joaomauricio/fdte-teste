import React from 'react';

const TextInput = ({ className, label, placeholder, name, value, onChange}) => {

    return (
        <div className={`${className} input__container`}>
            {label && (
                <label className="input__label">
                    {label}
                </label>
            )}
            <input value={value || ''} onChange={onChange} className="input" type="text" placeholder={placeholder} name={name} />
        </div>
    )
};

export default TextInput;
