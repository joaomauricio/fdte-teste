import React, {Component} from 'react';

import TextInput from './TextInput';
import NumberInput from './NumberInput';
import Dropdown from './Dropdown';
import Button from './Button';
import {pokemonTypes} from '../services/PokemonTypes.js';

import shield from '../assets/images/shield.png';
import speed from '../assets/images/speed.png';
import sword from '../assets/images/sword.png';

const typeOptions = Object.keys(pokemonTypes).map((e,i) => {
    return {
        text: e.toUpperCase(),
        value: e,
    }
});

class PokemonCreate extends Component {

    constructor(props) {
        super(props);

        let pokemon = {};
        if (props.pokemon) {
            pokemon = {
                name:props.pokemon.name,
                hp:props.pokemon.stats[0].base_stat,
                weight:props.pokemon.weight,
                type:props.pokemon.types[0].type.name,
                attack:props.pokemon.stats[1].base_stat,
                defense:props.pokemon.stats[2].base_stat,
                'special-defense':props.pokemon.stats[4].base_stat,
                'special-attack':props.pokemon.stats[3].base_stat,
                speed:props.pokemon.stats[5].base_stat,
                skill1:props.pokemon.abilities[0].ability.name,
                skill2:props.pokemon.abilities[1].ability.name,
                skill3:props.pokemon.abilities[2].ability.name,
                skill4:props.pokemon.abilities[3].ability.name,
                height:props.pokemon.height
            }
        }

        this.state = {
            pokemon: pokemon,
            formValidation: {

            },
            isFormValid: false
        }

        if (props.pokemon) {
            this.state.pokemon = pokemon;
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.checkIfFormIsValid = this.checkIfFormIsValid.bind(this);
        this.mapCustomPokemonToPokmeon = this.mapCustomPokemonToPokmeon.bind(this);
    }

    onCaptureAux(pokemon) {
        this.props.onCapture(pokemon)
    }

    handleSubmit(event) {
        event.preventDefault();
        this.onCaptureAux(this.mapCustomPokemonToPokmeon());
    };

    /*
        TODO: A solução ideal seria ter o form já produzindo um objeto
        que pudesse ser diretamente enviado para a bag, mas eu estava
        com problemas para manipular o estado dessa forma, e acabei
        optando por fazer assim devido ao prazo de entrega.
    */
    mapCustomPokemonToPokmeon() {
        return {
            // Variavel de controle necessária pois quando o pokemon
            // é custom created, todos os campos podem ser editados.
            isCustomCreated: true,
            name: this.state.pokemon.name,
            stats: [
                {
                    base_stat:this.state.pokemon.hp,
                    stat: {
                        name:'hp'
                    }
                },
                {
                    base_stat:this.state.pokemon.attack,
                    stat: {
                        name:'attack'
                    }
                },
                {
                    base_stat:this.state.pokemon.defense,
                    stat: {
                        name:'defense'
                    }
                },
                {
                    base_stat:this.state.pokemon.['special-attack'],
                    stat: {
                        name:'special-attack'
                    }
                },
                {
                    base_stat:this.state.pokemon.['special-defense'],
                    stat: {
                        name:'special-defense'
                    }
                },
                {
                    base_stat:this.state.pokemon.speed,
                    stat: {
                        name:'speed'
                    }
                }
            ],
            weight: this.state.pokemon.weight,
            height: this.state.pokemon.height,
            types:[{slot:1,type:{name:this.state.pokemon.type}}],
            abilities:[
                {
                    slot: 1,
                    ability:{
                        name: this.state.pokemon.skill1
                    }
                },
                {
                    slot: 2,
                    ability:{
                        name: this.state.pokemon.skill2
                    }
                },
                {
                    slot: 3,
                    ability:{
                        name: this.state.pokemon.skill3
                    }
                },
                {
                    slot: 4,
                    ability:{
                        name: this.state.pokemon.skill4
                    }
                },
            ],
            id: 9999
        }
    }

    handleChange(event) {
        const target = event.target;
        const newValue = target.value;
        const label = target.name;

        this.setState((prevState, event) => ({pokemon:{
            ...prevState.pokemon,
            [label]:newValue
        }}));
        this.validate(label, newValue);
        const isValid = this.checkIfFormIsValid()
        this.setState(() => ({isFormValid:isValid}))
    }

    // Essa validação não ficou muito elegante por falta de tempo para
    // implementar da melhor maneira.
    validate(label, newValue) {
        const isText = ['name','skill1','skill2','skill3', 'skill4'].indexOf(label) >= 0 ? true:false;
        const isNumber = !isText;

        if(label === 'type') {
            if (newValue === '' ) {
                this.setInputStatus(label, false);
            } else {
                this.setInputStatus(label, true);
            }
        }

        else if(isText && (newValue === '' || !isNaN(newValue))) {
            this.setInputStatus(label, false);
        } else if (isText) {
            this.setInputStatus(label, true);
        }

        else if(isNumber && (newValue === '' || isNaN(newValue))) {
            this.setInputStatus(label, false);
        } else if (isNumber) {
            this.setInputStatus(label, true);
        }
    }

    setInputStatus(label, isValid) {
        this.setState((prevState) => ({formValidation:{
            ...prevState.formValidation,
            [label]: isValid ? '':'danger'
        }}))
    }

    checkIfFormIsValid() {
        if (!this.state) {
            return false;
        }
        if (this.state.formValidation === {}) {
            return false;
        }

        if (Object.keys(this.state.formValidation).length >= 14) {
            return Object.values(this.state.formValidation).reduce((acum, e) => {
                return e === ''
            })
        } else {
            return false;
        }
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <TextInput className={this.state.formValidation.name} label='NOME' name='name' value={this.state.pokemon.name} onChange={this.handleChange}/>
                <NumberInput className={this.state.formValidation.hp} label='HP' name='hp' value={this.state.pokemon.hp} onChange={this.handleChange}/>
                <NumberInput className={this.state.formValidation.weight} label='PESO' name='weight' value={this.state.pokemon.weight} onChange={this.handleChange}/>
                <NumberInput className={this.state.formValidation.height} label='ALTURA' name='height' value={this.state.pokemon.height} onChange={this.handleChange}/>
                <div className='pokemon-stats'>
                    <h2><span>tipo</span></h2>
                </div>
                <Dropdown name='type' value={this.state.pokemon.type} onChange={this.handleChange} options={typeOptions}/>
                <div className='pokemon-stats'>
                    <h2><span>habildades</span></h2>
                </div>
                <TextInput className={this.state.formValidation.skill1} placeholder='HABILIDADE 1' name='skill1' value={this.state.pokemon.skill1} onChange={this.handleChange}/>
                <TextInput className={this.state.formValidation.skill2} placeholder='HABILIDADE 2' name='skill2' value={this.state.pokemon.skill2} onChange={this.handleChange}/>
                <TextInput className={this.state.formValidation.skill3} placeholder='HABILIDADE 3' name='skill3' value={this.state.pokemon.skill3} onChange={this.handleChange}/>
                <TextInput className={this.state.formValidation.skill4} placeholder='HABILIDADE 4' name='skill4' value={this.state.pokemon.skill4} onChange={this.handleChange}/>
                <div className='pokemon-stats'>
                    <h2><span>estatisticas</span></h2>
                </div>
                <NumberInput className={this.state.formValidation.defense} icon={shield} label='DEFESA' name='defense' value={this.state.pokemon.defense} onChange={this.handleChange}/>
                <NumberInput className={this.state.formValidation.attack} icon={sword} label='ATAQUE' name='attack'value={this.state.pokemon.attack} onChange={this.handleChange}/>
                <NumberInput className={this.state.formValidation.['special-defense']} icon={shield} label='DEFESA ESPECIAL' name='special-defense' value={this.state.pokemon.['special-defense']} onChange={this.handleChange}/>
                <NumberInput className={this.state.formValidation.['special-attack']} icon={sword} label='ATAQUE ESPECIAL' name='special-attack'value={this.state.pokemon.['special-attack']} onChange={this.handleChange}/>
                <NumberInput className={this.state.formValidation.speed} icon={speed} label='VELOCIDADE' name='speed' value={this.state.pokemon.speed} onChange={this.handleChange}/>
                <div type='submit' className='create'>
                    <Button disabled={!this.state.isFormValid} text='Criar Pokemon'/>
                </div>
            </form>
        )
    }

}

export default PokemonCreate;
