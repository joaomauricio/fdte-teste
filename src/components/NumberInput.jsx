import React from 'react';
import chevron from '../assets/images/chevronDownBlack.png';

const NumberInput = ({ className, label, placeholder, name, suffix, icon, value, onChange }) => {

    return (
        <div className='input__container'>
            {label && (
                <label className="input__label">
                    {icon && (<img alt='i' src={icon} style={{marginRight:'1rem'}}/>)}{label}
                </label>
            )}
            <div className={`${className} input__number`}>
                <input value={value || ''} onChange={onChange} className="input" type="text" placeholder={placeholder} name={name} />
                {suffix && (
                    <p className="input__suffix">
                        {suffix}
                    </p>
                )}
                <div className="input__btns">
                    <img src={chevron} className="input__increase" alt="Mais" />
                    <img src={chevron} className="input__decrease" alt="Menos" />
                </div>
            </div>
        </div>
    )
};

export default NumberInput;
