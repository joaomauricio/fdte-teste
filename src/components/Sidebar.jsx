import React from 'react';
import Button from './Button';
import plusIcon from '../assets/images/plus.png';


const Sidebar = (props) => {
    let sideBarContent = undefined;

    if (props.bag.length) {
        sideBarContent = props.bag.map((pokemon, index) => {
            return (
                <div onClick={() => props.onClickBag(pokemon)}className='sidebar__item' key={index}>
                    <img src={pokemon.sprites ? pokemon.sprites.front_default:''} alt='' />
                </div>
            )
        })
    } else {
        sideBarContent = (
            <div className="sidebar__item">
                ?
            </div>
        )
    }

    return (
        <div className="sidebar">
            {sideBarContent}
            <Button
                onClick={props.onClickCreate} icon={<img style={{height:'2rem'}} src={plusIcon} alt="+" />}
            />
        </div>
    );
};

export default Sidebar;
